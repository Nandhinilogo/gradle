package pages;

import org.openqa.selenium.WebElement;


import wdMethods.ProjectMethodsPOM;

public class MyLeadsPage extends ProjectMethodsPOM{
	
	public CreateLeadPage clickCreateLead()	{
		WebElement eleCreateLead = locateElement("linkText","Create Lead");
		click(eleCreateLead);
		return new CreateLeadPage();
		
	}
	
	

}

