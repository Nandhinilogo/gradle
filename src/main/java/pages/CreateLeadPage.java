package pages;

import org.openqa.selenium.WebElement;


import wdMethods.ProjectMethodsPOM;

public class CreateLeadPage extends ProjectMethodsPOM{
	
	public CreateLeadPage enterCompanyName(String uName)
	{
		WebElement eleCompanyName = locateElement("id","createLeadForm_companyName");
		type(eleCompanyName,uName);
		return this;
	}
	
	public CreateLeadPage enterFirstName(String firstName)
	{
		WebElement eleFirstName = locateElement("id","createLeadForm_firstName");
		type(eleFirstName,firstName);
		return this;
	}
	public CreateLeadPage enterLastName(String lastName)
	{
		WebElement eleLastName = locateElement("id","createLeadForm_lastName");
		type(eleLastName,lastName);
		return this;
	}
	
	public ViewPage clickCreateLead()	{
		WebElement eleCreadLead = locateElement("class","smallSubmit");
		click(eleCreadLead);
		return new ViewPage();
		
	}
	}
	
	



