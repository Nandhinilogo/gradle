package testCase;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.CreateLeadPage;
import pages.HomePage;
import pages.LoginPage;
import pages.MyLeadsPage;
import wdMethods.ProjectMethodsPOM;

public class TC002CreateLead extends ProjectMethodsPOM {
	@BeforeTest
	public void setData() {
		testCaseName = "TC001_CreateLead";
		testDesc = "Create a new Lead";
		author = "susila";
		category = "smoke";
		data1 = "AllFive";
		
	}
	
	@Test(dataProvider="fetchData")
	public void  openCRM(String username, String password,String firstName,String lastname,String Companyname)
	{
		
		new LoginPage()
		.enterUserName(username)
		.enterPassword(password)
		.clickLogin();
		
		new HomePage()
		.clickCRMSFA();
		
		new MyLeadsPage()
		.clickCreateLead();
		
		new CreateLeadPage()
		.enterLastName(firstName)
		.enterLastName(lastname)
		.enterCompanyName(Companyname)
		.clickCreateLead();
		
			}
	
	
}
