package testCase;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wdMethods.ProjectMethodsPOM;

public class TC001Login_Logout extends ProjectMethodsPOM {
	@BeforeTest
	public void setData() {
		testCaseName = "TC001_CreateLead";
		testDesc = "Create a new Lead";
		author = "susila";
		category = "smoke";
		data1 = "AllFive";
		
	}
	
	@Test(dataProvider="fetchData")
	public void  login(String username, String password){
		
		new LoginPage()
		.enterUserName(username)
		.enterPassword(password)
		.clickLogin();
				
	}
	
	
}
