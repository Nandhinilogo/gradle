package wdMethods;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;

import wdMethods.SeMethods;

public class projectMethods4 extends SeMethods{
	
	@Parameters({"url", "un","pass"})
	
	@BeforeMethod
	
	public void login(String url1,String un1, String pass1)
	{
	startApp("chrome", url1);
	WebElement eleUserName = locateElement("id", "username");
	type(eleUserName, un1);
	WebElement elePassword = locateElement("password");
	type(elePassword, pass1);
	WebElement eleLogin = locateElement("class","decorativeSubmit");
	click(eleLogin);
	//    CRM/SFA
	
	WebElement eleLink = locateElement("linkText", "CRM/SFA");
	click(eleLink);
	click(locateElement("linkText", "Create Lead"));
	}
	
	@AfterMethod
	public void closeApp()
	{
		closeBrowser();
	}
}

	