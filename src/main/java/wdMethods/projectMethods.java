package wdMethods;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;

import wdMethods.SeMethods;

public class projectMethods extends SeMethods{
	@BeforeSuite
	public void beforeSuite()
	{
		System.out.println("Before Suite");
	}
		
	@BeforeTest
	public void beforeTest()
	{
		System.out.println("Before Test called After BeforeSuite");
	}
	
	@BeforeClass
	public void beforeClass()
	{
		System.out.println("Before Class caled After BeforeTest");
	}
	
	@BeforeMethod
	
	public void login()
	{
	startApp("chrome", "http://leaftaps.com/opentaps");
	WebElement eleUserName = locateElement("id", "username");
	type(eleUserName, "DemoSalesManager");
	WebElement elePassword = locateElement("password");
	type(elePassword, "crmsfa");
	WebElement eleLogin = locateElement("class","decorativeSubmit");
	click(eleLogin);
	//    CRM/SFA
	
	WebElement eleLink = locateElement("linkText", "CRM/SFA");
	click(eleLink);
	click(locateElement("linkText", "Create Lead"));
	}
	
	@AfterMethod
	public void closeApp()
	{
		closeBrowser();
	}
	
	@AfterClass
	public void afterClass()
	{
		System.out.println("After Class called before AfterTest");
	}
	
	@AfterTest
	public void afterTest()
	{
		System.out.println("After Test called before AfterSuite");
	}
	
	@AfterSuite
	public void afterSuite()
	{
		System.out.println("After Suite Called Last");
	}
	



}
