package wdMethods;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;

import wdMethods.SeMethods;

public class projectMethods3 extends SeMethods{
	
	@BeforeMethod(groups="any")
	
	public void login()
	{
	startApp("chrome", "http://leaftaps.com/opentaps");
	WebElement eleUserName = locateElement("id", "username");
	type(eleUserName, "DemoSalesManager");
	WebElement elePassword = locateElement("password");
	type(elePassword, "crmsfa");
	WebElement eleLogin = locateElement("class","decorativeSubmit");
	click(eleLogin);
	//    CRM/SFA
	
	WebElement eleLink = locateElement("linkText", "CRM/SFA");
	click(eleLink);
	click(locateElement("linkText", "Create Lead"));
	}
	
	@AfterMethod(groups="any")
	public void closeApp()
	{
		closeBrowser();
	}
}

	