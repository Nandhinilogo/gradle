package testcases;

import java.io.IOException;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import wdMethods.ProjectMethodsCL;

public class TC424102018DL extends ProjectMethodsCL{
	
	@BeforeTest
	public void setData() {
		testCaseName = "TC005_DuplicateLead";
		testDesc = "Dublicate Leads";
		author = "Gopi";
		category = "Regression";
	}

	@Test(dataProvider="dup")
	public void duplicateLead(String ph) throws InterruptedException {
		click(locateElement("linkText", "Leads"));
		click(locateElement("linkText", "Find Leads"));
		click(locateElement("xpath", "//span[contains(text(),'Phone')]"));
		//type(locateElement("name", "phoneNumber"), ""+ph);
		type(locateElement("name", "phoneNumber"), ""+ph);
		click(locateElement("xpath", "//button[contains(text(),'Find Leads')]"));
		Thread.sleep(2000);
		String txt = getText(locateElement("xpath", "(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])[1]/a"));
		clickWithNoSnap(locateElement("xpath", "(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])[1]/a"));
		click(locateElement("linkText", "Duplicate Lead"));
		click(locateElement("xpath", "//input[@name='submitButton']"));				
	}
	
	
	@DataProvider(name = "dup")
	public Object[][] fetchdata() throws IOException
	{
		int x=3;
		//Object[][] data=readFile2.dataRead(x);
     	Object[][] data=ReadFile.dataRead(x);
		return data;
	}
	/*@DataProvider(name = "dup")
	public Object[][] fetchData3() {
		Object[][] data = new Object[2][1];
		
		data[0][0] = 9597704568l;
		data[1][0] = 9597704568l;
		
		return data;
	}
*/
}
